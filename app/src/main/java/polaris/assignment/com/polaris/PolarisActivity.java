package polaris.assignment.com.polaris;

import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;


public class PolarisActivity extends AppCompatActivity {
    private Toolbar toolbar;
    TextView txtTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_polaris);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        txtTitle = (TextView) findViewById(R.id.title);
        Typeface myCustomFont = Typeface.createFromAsset(getAssets(), "fonts/PunkRockShow.ttf");
        txtTitle.setTypeface(myCustomFont);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.menu_icon);
        //getSupportActionBar().setTitle("Trending Shows");
        //toolbar.setLogo(R.drawable.menu_trending_shows_default_crop);
        toolbar.getMenu().clear();
    }
}